<?php
include_once '../apporioconfig/start_up.php';
if(!isset($_SESSION['ADMIN']['ID']))
{
    $db->redirect("index.php");
}
include('common.php');

$query="select * from customer_support ORDER BY customer_support_id DESC";
$result = $db->query($query);
$list=$result->rows;
?>

<div class="wraper container-fluid">
    <div class="page-title">
        <h3 class="title">Customer Support</h3>
    </div>
    <div class="row">

        <div class="col-md-12 col-sm-12 col-xs-12 mobtbl">
            <table id="datatable" class="table table-striped table-bordered table-responsive">
                <thead>
                <tr>
                    <th width="5%">Sr.No.</th>
                    <th> Name</th>
                    <th>Email</th>
                    <th>Mobile</th>
                    <th>Query</th>
                    <th>Application</th>
                    <th>Date</th>
                </tr>
                </thead>
                <tbody>

                <?php $j = 1;
                foreach($list as $support){?>
                    <tr>
                        <td><?php echo $j;?></td>
                        <td>
                            <?php
                            $name = $support['name'];
                            echo $name;
                            ?>
                        </td>
                        <td>
                            <?php
                            $email = $support['email'];
                            if($email == "")
                            {
                                echo "------";
                            }
                            else
                            {
                                echo $email;
                            }
                            ?>
                        </td>
                        <td>
                            <?php
                            $phone = $support['phone'];
                            echo $phone;
                            ?>
                        </td>

                        <td>
                            <?php
                            $query=$support['query'];
                            echo  $query;

                            ?>

                        </td>


                        <td><?php
                            $application = $support['application'];
                            switch ($application){
                                case "1":
                                    echo "Customer";
                                    break;
                                case "2":
                                    echo "Driver";
                                    break;
                                default:
                                    echo "------";
                            }
                            ?></td>
                        <td><?php
                            $date = $support['date'];
                            echo $date;
                            ?></td>
                    </tr>
                    <?php  $j++;
                }
                ?>
                </tbody>

            </table>
        </div>


    </div>
    <!-- End row -->

</div>
</form>

</section>

</body></html>