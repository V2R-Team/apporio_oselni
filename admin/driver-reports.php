<?php
include_once '../apporioconfig/start_up.php';
if(!isset($_SESSION['ADMIN']['ID']))
{
    $db->redirect("index.php");
}
include('common.php');
$where = "";
if(isset($_POST['from']) && isset($_POST['to']))
{
    $start_date = $_POST['from'];
    $end_date = $_POST['to'];
    $where .= "and driver_signup_date >='$start_date'";
    $where .= "and driver_signup_date <='$end_date'";
}
if(isset($_POST['fixperiod']))
{

   $fixperiod = $_POST['fixperiod'];
   switch ($fixperiod){
       case "1":
           $start_date = date('Y-m-01',strtotime('this month'));
           $end_date = date("Y-m-t", strtotime($start_date));
       case "2":
           $startLastMonth = mktime(0, 0, 0, date("m") - 1, 1, date("Y"));
           $endLastMonth = mktime(0, 0, 0, date("m"), 0, date("Y"));
           $start_date = date("Y-m-d", $startLastMonth);
           $end_date = date("Y-m-d", $endLastMonth);
       case "3":
           $start_date = date('Y-m-d');
           $end_date = date('Y-m-d');
       case "4":
           $start_date = date('Y-m-d',strtotime("-1 days"));;
           $end_date = date('Y-m-d',strtotime("-1 days"));;
   }
   die();
}
$query="select * from city";
$result = $db->query($query);
$list1=$result->rows;
foreach ($list1 as $key=>$login)
{
    $city_id = $login['city_id'];
    $query = "select * from driver where 1=1 $where AND city_id='$city_id'";
    $result = $db->query($query);
    $driver_list=$result->num_rows;
    $list1[$key]=$login;
    $list1[$key]["total_driver"]=$driver_list;
}
$query="select * from car_type";
$result = $db->query($query);
$list=$result->rows;
foreach ($list as $key=>$login)
{
    $car_type_id = $login['car_type_id'];
    $query = "select * from driver where 1=1 $where AND car_type_id='$car_type_id'";
    $result = $db->query($query);
    $driver_list=$result->num_rows;
    $list[$key]=$login;
    $list[$key]["total_driver"]=$driver_list;
}

$query="select * from driver WHERE online_offline=1";
$result = $db->query($query);
$online =$result->num_rows;
$query="select * from driver WHERE online_offline !=1";
$result = $db->query($query);
$offline =$result->num_rows;
$query="select * from driver ORDER BY rating DESC LIMIT 10";
$result = $db->query($query);
$driver=$result->rows;
$query="select * from driver ORDER BY total_payment_eraned DESC LIMIT 10";
$result = $db->query($query);
$driver_amount = $result->rows;
$where1 = "";
if(isset($_POST['from']) && isset($_POST['to']))
{
    $start_date = $_POST['from'];
    $end_date = $_POST['to'];
    $where1 .= "and driver_earnings.date >='$start_date'";
    $where1 .= "and driver_earnings.date <='$end_date'";
}
$query = "select driver_earnings.amount,driver.driver_name from driver_earnings INNER JOIN driver ON driver_earnings.driver_id=driver.driver_id WHERE 1=1 $where1 ORDER BY driver_earnings.amount DESC LIMIT 10";
$result = $db->query($query);
$total_amount = $result->rows;

?>
<link href="css/calander.css" rel="stylesheet" />
<script src="js/calander_jquery.js"></script>
<script src="js/calander_jquery-ui.js"></script>
<script>
    var j = jQuery.noConflict();
    j(document).ready(function() {
        j("#from").datepicker({ dateFormat: 'yy-mm-dd' }).attr('readOnly', 'true');
        j("#to").datepicker({ dateFormat: 'yy-mm-dd'}).attr('readOnly', 'true');
    });
    function myFunction() {
        document.getElementById("fixperiod").disabled = true;
        document.getElementById("from").disabled = false;
        document.getElementById("to").disabled = false;
    }

    function unclickFunction() {
        document.getElementById("from").disabled = true;
        document.getElementById("to").disabled = true;
        document.getElementById("fixperiod").disabled = false;
    }

    function validatelogin() {
        if(document.getElementById('custom').checked == false && document.getElementById('fix').checked == false){
            alert("Select Serach Type");
            return false;
        }
        if(document.getElementById('custom').checked == true && document.getElementById('from').value == ""){
            alert("Select Date From");
            return false;
        }
        if(document.getElementById('custom').checked == true && document.getElementById('to').value == ""){
            alert("Select Date To");
            return false;
        }
        if(document.getElementById('fix').checked == true && document.getElementById('fixperiod').value == ""){
            alert("Select Date Period");
            return false;
        }
    }
</script>
<div class="wraper container-fluid" >
    <div class="page-title">
        <form method="post" onSubmit="return validatelogin()">
         <div class="form-group row" style="margin-bottom: 0px;">
            <div class="form-group col-xs-2">
                    <label><input type="radio" id="custom" name="custom" value="1" onclick="myFunction()">  Search From To</label>
            </div>
            <div class="col-xs-3">
                <input class="form-control" id="from" type="text" name="from">
            </div>
            <div class="col-xs-3">
                <input class="form-control" id="to" type="text" name="to">
            </div>
           </div>
            <div class="form-group row" style="margin-bottom: 0px;">
                <div class="form-group col-xs-2">
                    <label><input type="radio" name="custom" id="fix" value="1" onclick="unclickFunction()">  Search Period</label>
                </div>
                <div class="col-xs-3">
                    <select name="fixperiod" id="fixperiod" class="form-control">
                        <option value="1">This Month</option>
                        <option value="2">Last Month</option>
                        <option value="3">Today</option>
                        <option value="4">Yesterday</option>
                    </select>
                </div>
                <div class="col-xs-3">
                    <button style="float: inherit;" class="btn btn-primary" type="submit" name="search"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
                </div>
            </div>
        </form>
    </div>


    <div class="row col-md-12">

        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Driver Signup Comparison Month Wise</h3>
                </div>
                <div class="panel-body">
                    <div id="columnchart_values"  style="width: 500px; height: 400px;">
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Driver Car Type Comparison</h3>
                </div>
                <div class="panel-body">
                    <div id="piechart" style="width: 500px; height: 400px;">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row col-md-12">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Top Driver By Ratings</h3>
                </div>
                <div class="panel-body">
                    <div id="chart_div"  style="width: 500px; height: 400px;">
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Top Driver By Revenue</h3>
                </div>
                <div class="panel-body">
                    <div id="chart_div1"  style="width: 500px; height: 400px;"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="row col-md-12">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">City Wise Driver</h3>
                </div>
                <div class="panel-body">
                    <div id="city_div"  style="width: 500px; height: 400px;">
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Online Offline Driver</h3>
                </div>
                <div class="panel-body">
                    <div id="online_div"  style="width: 500px; height: 400px;"></div>
                </div>
            </div>
        </div>
    </div>


</div>


<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<script type="text/javascript">
    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawChart);
    function drawChart() {
        var data = google.visualization.arrayToDataTable([
            ['Car Type', 'Driver Per Car Type'],
            <?php foreach ($list as $data){?>
            ['<?php echo $data['car_type_name']?>', <?php echo $data['total_driver']?>],
            <?php } ?>
        ]);

        var options = {
            is3D: true,
        };
        var chart = new google.visualization.PieChart(document.getElementById('piechart'));
        chart.draw(data,options);
    }

</script>
<script type="text/javascript">
    google.charts.load("current", {packages:['corechart']});
    google.charts.setOnLoadCallback(drawChart);
    function drawChart() {
        var data = google.visualization.arrayToDataTable([
            ["Month", "User Per Month", { role: "style" } ],
            ["January", 10, "#3F33FF"],
            ["February", 100, "#3F33FF"],
            ["March", 10, "#3F33FF"],
            ["May", 200, "color: #3F33FF"],
            ["June", 50, "color: #3F33FF"],
            ["July", 125, "color: #3F33FF"],
            ["August", 70, "color: #3F33FF"],
            ["September", 75, "color: #3F33FF"],
            ["October", 80, "color: #3F33FF"],
            ["November", 90, "color: #3F33FF"],
            ["December", 60, "color: #3F33FF"],
        ]);

        var view = new google.visualization.DataView(data);
        view.setColumns([0, 1,
            { calc: "stringify",
                sourceColumn: 1,
                type: "string",
                role: "annotation" },
            2]);

        var options = {
            width: 600,
            height: 400,
            bar: {groupWidth: "50%"},
            legend: { position: "none" },
        };
        var chart = new google.visualization.ColumnChart(document.getElementById("columnchart_values"));
        chart.draw(view, options);
    }
</script>
<script>
    google.charts.load('current', {packages: ['corechart', 'bar']});
    google.charts.setOnLoadCallback(drawBasic);

    function drawBasic() {

        var data = google.visualization.arrayToDataTable([
            ['Drivers', 'Driver Rating Points',{ role: 'style' }],
            <?php foreach ($driver as $value){?>
            ['<?php echo $value['driver_name']?>', <?php echo $value['rating']?>,'silver'],
            <?php } ?>
        ]);

        var options = {
            chartArea: {width: '50%'},
            hAxis: {
                title: 'Total Rating',
                minValue: 0
            },
            vAxis: {
                title: 'Drivers'
            }
        };

        var chart = new google.visualization.BarChart(document.getElementById('chart_div'));
        chart.draw(data,options);
    }
</script>
<script>
    google.charts.load('current', {packages: ['corechart', 'bar']});
    google.charts.setOnLoadCallback(drawBasic);

    function drawBasic() {

        var data = google.visualization.arrayToDataTable([
            ['Drivers', 'Driver Revenue Money',{ role: 'style' }],
            <?php foreach ($total_amount as $value){?>
            ['<?php echo $value['driver_name']?>', <?php echo $value['amount']?>,'#520D0D'],
            <?php } ?>
        ]);

        var options = {
            chartArea: {width: '50%'},
            hAxis: {
                title: 'Revenue Money',
                minValue: 0
            },
            vAxis: {
                title: 'Drivers'
            }
        };

        var chart = new google.visualization.BarChart(document.getElementById('chart_div1'));
        chart.draw(data,options);
    }
</script>
<script type="text/javascript">
    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawChart);
    function drawChart() {
        var data = google.visualization.arrayToDataTable([
            ['City Name', 'Driver Per City'],
            <?php foreach ($list1 as $data){?>
            ['<?php echo $data['city_name']?>', <?php echo $data['total_driver']?>],
            <?php } ?>
        ]);

        var options = {
            is3D: true,
        };
        var chart = new google.visualization.PieChart(document.getElementById('city_div'));
        chart.draw(data,options);
    }

</script>
<script type="text/javascript">
    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawChart);
    function drawChart() {
        var data = google.visualization.arrayToDataTable([
            ['Online Offline', 'Driver Online Offline'],
            ['Online', <?php echo $online?>],
            ['Offline', <?php echo $offline?>]
        ]);

        var options = {
            is3D: true,
        };
        var chart = new google.visualization.PieChart(document.getElementById('online_div'));
        chart.draw(data,options);
    }

</script>
</section>
</body></html>
