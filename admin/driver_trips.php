<?php
include_once '../apporioconfig/start_up.php';
if(!isset($_SESSION['ADMIN']['ID']))
{
$db->redirect("index.php");
}
include('common.php');
$query = "select * from driver where driver_id='".$_GET['driver_id']."'";
$result = $db->query($query);
$driver = $result->row;

$query = "select * from table_user_rides INNER JOIN user ON table_user_rides.user_id=user.user_id where table_user_rides.driver_id='".$_GET['driver_id']."'";
$result = $db->query($query);
$list = $result->rows;
foreach ($list as $keys=>$login)
{
    $ride_mode = $login['ride_mode'];
    $booking_id = $login['booking_id'];
    if ($ride_mode == 1)
    {
        $query = "select * from ride_table INNER JOIN car_type ON ride_table.car_type_id=car_type.car_type_id where ride_table.ride_id='$booking_id'";
        $result = $db->query($query);
        $normal_ride = $result->row;
        $pickup_location = $normal_ride['pickup_location'];
        $drop_location = $normal_ride['drop_location'];
        $car_type_name = $normal_ride['car_type_name'];
        $booking_date = $normal_ride['ride_date'];
        $ride_status = $normal_ride['ride_status'];
        $query = "select * from done_ride where ride_id='$booking_id'";
        $result = $db->query($query);
        $done_ride = $result->row;
        $final_bill_amount = $done_ride['total_amount'];
    }else{
        $query = "select * from rental_booking INNER JOIN car_type ON rental_booking.car_type_id=car_type.car_type_id where rental_booking.rental_booking_id='$booking_id'";
        $result = $db->query($query);
        $rental_booking = $result->row;
        $pickup_location = $rental_booking['pickup_location'];
        $car_type_name = $rental_booking['car_type_name'];
        $booking_date = $rental_booking['booking_date'];
        $ride_status = $rental_booking['booking_status'];
        $query = "select * from table_done_rental_booking where rental_booking_id='$booking_id'";
        $result = $db->query($query);
        $done_rental_booking = $result->row;
        $drop_location = $done_rental_booking['end_location'];
        $final_bill_amount = $done_rental_booking['final_bill_amount'];
    }
    $list[$keys]=$login;
    $list[$keys]["pickup_location"]=$pickup_location;
    $list[$keys]["drop_location"] = $drop_location;
    $list[$keys]["car_type_name"]= $car_type_name;
    $list[$keys]["booking_date"]= $booking_date;
    $list[$keys]["ride_status"]= $ride_status;
    $list[$keys]["final_bill_amount"]= $final_bill_amount;
}
?>
<div class="wraper container-fluid">
    <div class="page-title">
        <h3 class="title"><?php
            $driver_name = $driver['driver_name'];
            echo $driver_name;
            ?> Rides</h3>
    </div>
    <div class="row">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 mobtbl">
                            <table id="datatable" class="table table-striped table-bordered table-responsive">
                                <thead>
                                <tr>
                                    <th>Sr.No</th>
                                    <th>Ride Type</th>
                                    <th>User Name</th>
                                    <th>Pickup Location</th>
                                    <th>Drop Location</th>
                                    <th>Car Type</th>
                                    <th>Ride Date</th>
                                    <th>Ride Status</th>
                                    <th>Bill Amount</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $j = 1;
                                foreach($list as $rides){ ?>
                                    <tr>
                                        <td>
                                            <?php
                                            echo $j;
                                            ?>
                                        </td>
                                        <td><?php $ride_mode = $rides['ride_mode'];
                                            if ($ride_mode == 1)
                                            {
                                                echo "Normal Ride";
                                            }else{
                                                echo "Rental Ride";
                                            }
                                            ?></td>
                                        <td><?php $user_name = $rides['user_name'];
                                            echo $user_name;
                                            ?></td>
                                        <td>
                                            <?php
                                            $pickup_location = $rides['pickup_location'];
                                            echo $pickup_location;
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                            $drop_location = $rides['drop_location'];
                                            echo $drop_location ;
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                            $car_type_name = $rides['car_type_name'];
                                            echo $car_type_name;
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                            $booking_date = $rides['booking_date'];
                                            echo $booking_date;
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                            $ride_status = $rides['ride_status'];
                                            switch ($ride_status){
                                                case "1":
                                                    echo nl2br("New Booking ");
                                                    break;
                                                case "2":
                                                    echo nl2br("Cancelled By User ");
                                                    break;
                                                case "3":
                                                    echo nl2br("Accepted by Driver");
                                                    break;
                                                case "4":
                                                    echo nl2br("Cancelled by driver ");
                                                    break;
                                                case "5":
                                                    echo nl2br("Driver Arrived ");
                                                    break;
                                                case "6":
                                                    echo nl2br("Trip Started ");
                                                    break;
                                                case "7":
                                                    echo nl2br("Trip Completed  ");
                                                    break;
                                                case "8":
                                                    echo nl2br("Trip Book By Admin  ");
                                                    break;
                                                case "17":
                                                    echo nl2br("Trip Cancel By Admin");
                                                    break;
                                                case "10":
                                                    echo nl2br("New Booking");
                                                    break;
                                                case "11":
                                                    echo nl2br("Accepted by Drive  ");
                                                    break;
                                                case "12":
                                                    echo nl2br("Driver Arrived  ");
                                                    break;
                                                case "13":
                                                    echo nl2br("Trip Started  ");
                                                    break;
                                                case "14":
                                                    echo nl2br("Trip Reject By Driver  ");
                                                    break;
                                                case "15":
                                                    echo nl2br("Trip Cancel By User  ");
                                                    break;
                                                case "16":
                                                    echo nl2br("Trip Completed ");
                                                    break;
                                                default:
                                                    echo "----";
                                            }
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                            $final_bill_amount = $rides['final_bill_amount'];
                                            echo $final_bill_amount;
                                            ?>
                                        </td>

                                    </tr>
                                <?php
                                    $j++;
                                }?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
    </div>
</div>
