<?php
include_once '../apporioconfig/start_up.php';
if(!isset($_SESSION['ADMIN']['ID']))
{
    $db->redirect("index.php");
}
include('common.php');

$query='SELECT * FROM languages';
$result=$db->query($query);
$list=$result->rows;
if(isset($_GET['lang']))
{
    $language_code = $_GET['lang'];
}else{
    $language_code = "en";
}
$query2="SELECT * FROM table_messages WHERE language_code='$language_code'";
$result2=$db->query($query2);
$message=$result2->rows;

$query2="SELECT * FROM messages WHERE language_id=1";
$result2=$db->query($query2);
$list2=$result2->rows;
if(isset($_POST['save'])) {
    $query2="SELECT * FROM table_messages WHERE language_code='$language_code'";
    $result2=$db->query($query2);
    $message=$result2->row;
    $data_message = $_POST;
    unset($data_message['save']);
    if (empty($message))
    {
        foreach($data_message as $key => $value) {
            $query2="INSERT INTO table_messages (message_id,message,language_code) VALUES ('$key','$value','$language_code')";
            $db->query($query2);
        }
    }else{
        foreach($data_message as $key => $value) {
            $query3="UPDATE table_messages SET message ='$value' WHERE message_id='$key' AND language_code='$language_code'";
            $db->query($query3);
        }
    }
    $db->redirect("home.php?pages=edit-languages&lang=$language_code");
}
?>



    <div class="wraper container-fluid">
        <div class="page-title">
            <h3 class="title">Language Manegement</h3>
        </div>
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading"><div class="row"><h2 class="col-md-4"> Edit Application Messages</h2>
                    <div class="col-md-8" align="right">Select a language :
                        <form method="post">
                        <select class="bootstrap-select" name="select_language" style="height: 50px" onchange="location=this.value;">
                            <option disabled selected>Language Name</option>
                            <?php foreach ($list as $language){ ?>
                                <option value="home.php?pages=edit-languages&lang=<?php echo $language['iso_639-1']?>" <?php if ($language['iso_639-1'] == $language_code){ ?> selected <?php } ?>><?php echo $language['name']?></option>
                            <?php } ?>
                        </select>
                    </form>
                    </div>
                </div>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <form method="post">
                                <?php foreach ($list2 as $data1){ ?>
                                    <div class="col-md-5 txtbx">
                                        <lable class="txthead"><?php
                                            $msg = $data1['message_name'];
                                            echo strtoupper(''.$msg);?>
                                        </lable>
                                        <input class="form-control " name="<?php echo $data1['message_id'];?>" value="<?php foreach ($message as $msg){
                                            if($data1['message_id'] == $msg['message_id']){
                                                echo $msg['message'];
                                            }
                                        }?>" required>
                                    </div>
                                <?php } ?>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group" style="text-align: center;">
                                            <button class="btn btn-danger" name="save" type="submit">Save</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
        </div>
        <!-- End row -->

    </div>
</form>
</section>
<!-- Main Content Ends -->

</body></html>