<?php
include_once '../apporioconfig/start_up.php';
if(!isset($_SESSION['ADMIN']['ID']))
{
    $db->redirect("index.php");
}
include('common.php');
$query ="select * from rental_payment ORDER BY rental_payment_id DESC";
$result = $db->query($query);
$list = $result->rows;
foreach ($list as $key=>$value)
{
    $rental_booking_id = $value['rental_booking_id'];
    $query="select * from rental_booking WHERE rental_booking_id='$rental_booking_id'";
    $result = $db->query($query);
    $list2=$result->row;
    $user_id = $list2['user_id'];
    $payment_option_id = $list2['payment_option_id'];
    $query="select * from table_done_rental_booking WHERE rental_booking_id='$rental_booking_id'";
    $result = $db->query($query);
    $list1=$result->row;
    $rental_package_price = $list1['final_bill_amount'];
    $coupan_price = $list1['coupan_price'];
    $driver_id = $list1['driver_id'];

    $query1 = "select * from payment_option where payment_option_id ='$payment_option_id'";
    $result1 = $db->query($query1);
    $list12 = $result1->row;
    $payment_option_name = $list12['payment_option_name'];
    $query="select * from driver WHERE driver_id='$driver_id'";
    $result = $db->query($query);
    $list4=$result->row;
    $driver_name = $list4['driver_name'];
    $city_id = $list4['city_id'];
    $query="select * from city WHERE city_id='$city_id'";
    $result = $db->query($query);
    $list5=$result->row;
    $currency = $list5['currency'];

    $user_booking_date_time = $list2['user_booking_date_time'];
    $query="select * from user WHERE user_id='$user_id'";
    $result = $db->query($query);
    $list3=$result->row;
    $user_name = $list3['user_name'];

    $list[$key] = $value;
    $list[$key]["user_booking_date_time"] = $user_booking_date_time;
    $list[$key]["user_name"] = $user_name;
    $list[$key]["driver_name"] = $driver_name;
    $list[$key]["currency"] = $currency;
    $list[$key]["rental_package_price"] = $rental_package_price;
    $list[$key]["coupan_price"] = $coupan_price;
    $list[$key]["payment_method"] = $payment_option_name;

}
?>
<div class="wraper container-fluid">
    <div class="page-title">
        <h3 class="title">Rental Transactions</h3>
        <span>
            <a href="home.php?pages=export-rental-transactions" class="btn btn-primary btn-lg" id="add-button"  title="Export Transactions" role="button">Export Transactions</a>
           </span>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 mobtbl">
                            <table id="datatable" class="table table-striped table-bordered table-responsive">
                                <thead>
                                <tr>
                                    <th>Sr.No</th>
                                    <th>User Name</th>
                                    <th>Driver Name</th>
                                    <th>Total Amount</th>
                                    <th>Package Price</th>
                                    <th>Coupan Price</th>
                                    <th>Payment Mode</th>
                                    <th>Date</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $j = 1;
                                foreach($list as $payment_confirm){
                                    ?>
                                    <tr>
                                        <td><?php
                                            echo $j;
                                            ?></td>
                                        <td><?php
                                            $begin_location = $payment_confirm['user_name'];
                                            if ($begin_location == "") {
                                                echo "------";
                                            } else {
                                                echo $begin_location;
                                            }
                                            ?></td>
                                        <td><?php
                                            $end_location = $payment_confirm['driver_name'];
                                            if ($end_location == "") {
                                                echo "------";
                                            } else {
                                                echo $end_location;
                                            }
                                            ?></td>
                                        <td><?php
                                            $currency = $payment_confirm['currency'];
                                            $payment_amount = $payment_confirm['amount_paid'];
                                            if ($payment_amount == "") {
                                                echo "------";
                                            } else {
                                                echo $currency . " " . $payment_amount;
                                            }
                                            ?></td>
                                        <td><?php
                                            $currency = $payment_confirm['currency'];
                                            $rental_package_price = $payment_confirm['rental_package_price'];
                                            if ($rental_package_price == "") {
                                                echo "------";
                                            } else {
                                                echo $currency . " " . $rental_package_price;
                                            }
                                            ?></td>

                                        <td><?php
                                            $currency = $payment_confirm['currency'];
                                            $coupan_price = $payment_confirm['coupan_price'];
                                            if ($coupan_price == "") {
                                                echo "------";
                                            } else {
                                                echo $currency . " " . $coupan_price;
                                            }
                                            ?></td>
                                        <td><?php
                                            $payment_method = $payment_confirm['payment_method'];
                                            if ($payment_method == "") {
                                                echo "------";
                                            } else {
                                                echo  $payment_method;
                                            }
                                            ?></td>
                                        <td><?php
                                            $user_booking_date_time = $payment_confirm['user_booking_date_time'];
                                            if ($user_booking_date_time == "") {
                                                echo "------";
                                            } else {
                                                echo $user_booking_date_time;
                                            }
                                            ?></td>
                                    </tr>
                                    <?php $j++;
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>