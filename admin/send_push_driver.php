<?php
function IphonePushNotificationCustomer($did,$msg) 
{
	$deviceToken = $did;
	$passphrase = 'programmer';
	$message = $msg;

	$ctx = stream_context_create();
	stream_context_set_option($ctx, 'ssl', 'local_cert', 'taxi_user_debug.pem');
	stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

	// Open a connection to the APNS server
	$fp = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $err,$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

	if (!$fp)
	exit("Failed to connect: $err $errstr" . PHP_EOL);

	//echo 'Connected to APNS' . PHP_EOL;

	// Create the payload body
	$body['aps'] = array('alert' => $message,'sound' => 'default');

	// Encode the payload as JSON
	$payload = json_encode($body);

	// Build the binary notification
	$msg = chr(0) . pack('n', 32) . pack('H*', $did) . pack('n', strlen($payload)) . $payload;

	// Send it to the server
	$result = fwrite($fp, $msg, strlen($msg));

	// Close the connection to the server
	fclose($fp);
}

function IphonePushNotificationDriver($did,$msg) 
{
	$deviceToken = $did;
	$passphrase = 'programmer';
	$message = $msg;

	$ctx = stream_context_create();
	stream_context_set_option($ctx, 'ssl', 'local_cert', 'taxi_driver_debug.pem');
	stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

	// Open a connection to the APNS server
	$fp = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $err,$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

	if (!$fp)
	exit("Failed to connect: $err $errstr" . PHP_EOL);

	//echo 'Connected to APNS' . PHP_EOL;

	// Create the payload body
	$body['aps'] = array('alert' => $message,'sound' => 'default');

	// Encode the payload as JSON
	$payload = json_encode($body);

	// Build the binary notification
	$msg = chr(0) . pack('n', 32) . pack('H*', $did) . pack('n', strlen($payload)) . $payload;

	// Send it to the server
	$result = fwrite($fp, $msg, strlen($msg));

	// Close the connection to the server
	fclose($fp);
}