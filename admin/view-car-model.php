<?php
include_once '../apporioconfig/start_up.php';
if(!isset($_SESSION['ADMIN']['ID']))
{
    $db->redirect("index.php");
}
include('common.php');

    $query123 = "select * from car_model INNER JOIN car_type ON car_model.car_type_id=car_type.car_type_id ORDER BY car_model_id DESC";
	$result123 = $db->query($query123);
     $list = $result123->rows;

   if(isset($_GET['status']) && isset($_GET['id'])) 
    {
     $query1="UPDATE car_model SET car_model_status='".$_GET['status']."' WHERE car_model_id='".$_GET['id']."'";
     $db->query($query1);
     $db->redirect("home.php?pages=view-car-model");
    }

      
      if(isset($_POST['savechanges'])) 
     {   
         if($_FILES['car_model_image']['name'] == ""){
           $query2="UPDATE car_model SET car_model_name='".$_POST['car_model_name']."',car_model_name_french='".$_POST['car_model_french']."',car_type_id='".$_POST['car_type_id']."',car_make='".$_POST['car_make']."',car_model='".$_POST['car_model']."',car_year='".$_POST['car_year']."',car_color='".$_POST['car_color']."' where car_model_id='".$_POST['savechanges']."'";
           $db->query($query2); 
           $db->redirect("home.php?pages=view-car-model");
         }else{
          $img_name = $_FILES['car_model_image']['name'];
          $filedir  = "../uploads/car/";
          if(!is_dir($filedir)) mkdir($filedir, 0755, true);
          $fileext = strtolower(substr($_FILES['car_model_image']['name'],-4));
          if($fileext==".jpg" || $fileext==".gif" || $fileext==".png" || $fileext=="jpeg") 
          {
          if($fileext=="jpeg") 
           {
           $fileext=".jpg";
           }
           $pfilename = "editcar_".$_POST['savechanges'].$fileext;
           $filepath1 = "uploads/car/".$pfilename;
           $filepath = $filedir.$pfilename;
           copy($_FILES['car_model_image']['tmp_name'], $filepath);      
           $query2="UPDATE car_model SET car_model_name='".$_POST['car_model_name']."',car_model_name_french='".$_POST['car_model_french']."', car_type_id='".$_POST['car_type_id']."',car_model_image='$filepath1',car_make='".$_POST['car_make']."',car_model='".$_POST['car_model']."',car_year='".$_POST['car_year']."',car_color='".$_POST['car_color']."' where car_model_id='".$_POST['savechanges']."'";
           $db->query($query2); 
           $db->redirect("home.php?pages=view-car-model");
        }
      }
     }   
        
        
?>
  <form method="post" name="frm">
  <div class="wraper container-fluid">
    <div class="page-title">
      <h3 class="title">View Vehicle Model</h3>
        <span>
            <a href="home.php?pages=add-car-model" class="btn btn-default btn-lg" id="add-button" title="Add A Vehicle Model" role="button">Add Vehicle Model</a>
      </span>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="panel panel-default">
          <div class="panel-body">
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12 mobtbl">
                
              <table id="datatable" class="table table-striped table-bordered table-responsive">
                    <thead>
                      <tr>
                        <th>S.No</th>
                        <th>Vehicle Type</th>
                        <th>Model Name</th>
                         <th>Model Name In French</th>
                        <th>Image</th>
                        <th width="12%">Status</th>
                        <th width="4%">Edit</th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php $dummyImg="http://apporio.co.uk/apporiotaxi/uploads/car/cardummy.png";
                      foreach($list as $viewmodel){?>
                    <tr>
                      <td><?php echo $viewmodel['car_model_id'];?></td>
                      <td><?php
                          $car_type_name = $viewmodel['car_type_name'];
                          echo $car_type_name;
                           ?>
                      </td>
                      <td><?php
                           $carmodel = $viewmodel['car_model_name'];
                           echo $carmodel;
                         ?>
                      </td>
                      
                      <td><?php
                           $car_model_name_french = $viewmodel['car_model_name_french'];
                           if($car_model_name_french=="")
                           {
                           echo "---------";
                           }
                           else
                           {
                           echo $car_model_name_french;
                           }
                         ?>
                      </td>
                      <td><img src="<?php if($viewmodel['car_model_image'] != '' && isset($viewmodel['car_model_image'])){echo '../'.$viewmodel['car_model_image'];}else{ echo $dummyImg; }?>"  width="70px" height="70px"></td>                      
                      
                      
                      
                      <?php
                                if($viewmodel['car_model_status']==1) {
                                ?>
                                <td class="text-center">
                                    <a href="home.php?pages=view-car-model&status=2&id=<?php echo $viewmodel['car_model_id']?>" class="" title="Active">
                                    <button type="button" class="btn btn-success br2 btn-xs fs12 activebtn" > Active
                                    </button></a>
                                </td>
                                <?php
                                } else {
                                ?>
                                <td class="text-center">
                                <a href="home.php?pages=view-car-model&status=1&id=<?php echo $viewmodel['car_model_id']?>" class="" title="Deactive">
                                    <button type="button" class="btn btn-danger  br2 btn-xs fs12 dropdown-toggle" > Deactive
                                    </button></a>
                                </td>
                                <?php } ?>
                                
 <td><button type="button" class="btn btn-info glyphicon glyphicon-pencil" data-toggle="modal" data-target="#<?php echo $viewmodel['car_model_id']?>"  ></button></td>                      
                      
                      
                    </tr>
                    <?php }?>
                    </tbody>
                    
                  </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- End row --> 
    
  </div>
  </form>
  <!-- Page Content Ends --> 
<?php foreach($list as $viewmodel){?>
<div class="modal fade" id="<?php echo $viewmodel['car_model_id'];?>" role="dialog">
  <div class="modal-dialog"> 
    
    <!-- Modal content starts-->
    
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title fdetailsheading">Edit Car Model Details</h4>
      </div>
      <form  method="post"  enctype="multipart/form-data" onSubmit="return validatelogin()">
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <div class="form-group">
              <label for="field-3" class="control-label">Car Type</label>
              <select class="form-control" name="car_type_id" id="car_type_id" required>
                <?php
                $car_type_id = $viewmodel['car_type_id'];
                $car_type_name = $viewmodel['car_type_name'];
              ?>
                 <option value="<?php echo $car_type_id;?>"><?php echo $car_type_name; ?></option>
            <?php foreach($list123 as $car) :?>
                           <option value="<?php echo $car['car_type_id'];?>"><?php echo $car['car_type_name']; ?></option>
                <?php endforeach; ?>
    					
					</select>
            </div>
          </div>
          
           <div class="col-md-12">
            <div class="form-group">
              <label for="field-3" class="control-label">Model Name</label>
              <input type="text" class="form-control"  placeholder="Model Name" name="car_model_name" value="<?php echo $viewmodel['car_model_name'];?>" id="car_model_name" required>
            </div>
          </div>


          <div class="col-md-12">
            <div class="form-group">
              <label for="field-3" class="control-label">Model Name In French</label>
              <input type="text" class="form-control"  placeholder="Model Name In French" name="car_model_french" value="<?php echo $viewmodel['car_model_name_french'];?>" id="car_model_french" required>
            </div>
          </div>

            <div class="col-md-12">
                <div class="form-group">
                    <label for="field-3" class="control-label">Make</label>
                    <input type="text" class="form-control"  placeholder="Make" name="car_make" value="<?php echo $viewmodel['car_make'];?>" id="car_make" required>
                </div>
            </div>

            <div class="col-md-12">
                <div class="form-group">
                    <label for="field-3" class="control-label">Model</label>
                    <input type="text" class="form-control"  placeholder="Model" name="car_model" value="<?php echo $viewmodel['car_model'];?>" id="car_model" required>
                </div>
            </div>

            <div class="col-md-12">
                <div class="form-group">
                    <label for="field-3" class="control-label">Year</label>
                    <input type="text" class="form-control"  placeholder="Year" name="car_year" value="<?php echo $viewmodel['car_year'];?>" id="car_year" required>
                </div>
            </div>

            <div class="col-md-12">
                <div class="form-group">
                    <label for="field-3" class="control-label">Color</label>
                    <input type="text" class="form-control"  placeholder="Color" name="car_color" value="<?php echo $viewmodel['car_color'];?>" id="car_color" required>
                </div>
            </div>


            <div class="col-md-12">
              <div class="form-group">
                <label for="field-3" class="control-label">Car Model Image</label>
                <input type="file" name="car_model_image" id="car_model_image" />
              </div>
            </div>
          
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
        <button type="submit" name="savechanges" value="<?php echo $viewmodel['car_model_id'];?>" class="btn btn-info">Save Changes</button>
      </div>
      </form>
    </div>
  </div>
</div>
<?php }?>
  <!-- ================== --> 
  
</section>
<!-- Main Content Ends -->

</body>
</html>
