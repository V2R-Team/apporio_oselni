<?php
include_once '../apporioconfig/start_up.php';
header("Content-Type: application/json");

$driver_id=$_REQUEST['driver_id'];

if($driver_id!="")
{
        $query="SELECT * FROM driver WHERE driver_id='$driver_id'";
		$result = $db->query($query);
		$list=$result->row;
		$commision = $list['commission'];
		if(!empty($list)){
		$company_payment = $list['company_payment'];
	    $driver_payment = $list['driver_payment'];;
		$total_payment_eraned = $list['total_payment_eraned'];
		if($total_payment_eraned == "")
		{
		 $total_payment_eraned = "00";
		}
		$date = date("Y-m-d");
		$ts = strtotime($date);
		
		$dow = date('w', $ts);
		$offset = $dow - 1;
		if ($offset < 0) {
		    $offset = 6;
		}
		
		$ts = $ts - $offset*86400;
		
		for ($i = 0; $i < 7; $i++, $ts += 86400){
			$a[]= date("Y-m-d l", $ts);
		}
		foreach($a as $v)
		{
		  $date = explode(" ",$v);
		  $b[]= array('date'=>$date[0],'day'=>$date[1]);
		}
		$weekly_amount=0;
		$total_rides=0;
		$total_weekly_amount = 0;
		foreach($b as $data)
		{
		         $d = $data['date'];
		         $day= $data['day'];
			 $query="SELECT * FROM driver_earnings WHERE driver_id='$driver_id' AND date='$d'";
		         $result = $db->query($query);
			 $list1=$result->row;
			 if(empty($list1))
			 {
			   $list1 = array('driver_earning_id'=>"0",'driver_id'=>$driver_id,'total_amount'=>"0",'rides'=>"0",'amount'=>"0",'date'=>$d);
			 }
			 $weekly_amount = $weekly_amount+$list1['amount'];
			 $total_weekly_amount = $total_weekly_amount+$list1['total_amount'];
			 $total_rides = $total_rides+$list1['rides'];
			 $c[] = array('date'=>$d,'day'=>$day,'detail'=>$list1);
		}
		if($commision != 0)
		{
			$company_cut = ($total_weekly_amount*$commision)/100;
			$company_cut=number_format((float)$company_cut, 2, '.', '');
			$fare_recevied = (string)$total_weekly_amount;
		}else{
			$company_cut = "0";
			$fare_recevied = (string)$total_weekly_amount;
		}
		$re = array('result' => 1,'msg'	=>"Weekly Earning",'fare_recevied'=>$fare_recevied,'company_cut'=>$company_cut,'total_payment_eraned'=>$total_payment_eraned,'weekly_amount'=>$weekly_amount,'company_payment'=>$company_payment,'driver_payment'=>$driver_payment,'total_rides'=>$total_rides,'details'=>$c);
		}else{
		$re = array('result' => 0,'msg'	=>"Wrong Driver Id");
		}
		
}else{
	$re = array('result' => 0,'msg'	=>"Required Field Missing");
}
echo json_encode($re, JSON_PRETTY_PRINT);
?>