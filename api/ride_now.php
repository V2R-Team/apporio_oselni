<?php
error_reporting(0);
include_once '../apporioconfig/start_up.php';
header("Content-Type: application/json");

include 'pn_android.php';
include 'pn_iphone.php';
include 'firebase_new.php';

$user_id = $_REQUEST['user_id'];
$coupon_code = $_REQUEST['coupon_code'];
$pickup_lat = $_REQUEST['pickup_lat'];
$pickup_long = $_REQUEST['pickup_long'];
$pickup_location = $_REQUEST['pickup_location'];
$drop_lat = $_REQUEST['drop_lat'];
$drop_long = $_REQUEST['drop_long'];
$drop_location = $_REQUEST['drop_location'];
$car_type_id=$_REQUEST['car_type_id'];
//$language_id=$_REQUEST['language_id'];
$language_id=1;
$payment_option_id=$_REQUEST['payment_option_id'];
$card_id=$_REQUEST['card_id'];
$pem_file = $_REQUEST['pem_file'];
if($user_id !="" && $pickup_lat !="" && $pickup_long !="" && $pickup_location !="" &&  $car_type_id !="" && $payment_option_id !="")
{
    if($pem_file == ""){
        $pem_file = 1;
    }

        $time = date("H:i:s");
        $dt = DateTime::createFromFormat('!d/m/Y', date("d/m/Y"));
        $data=$dt->format('M j');
        $day=date("l");
        $date1=$day.", ".$data ;

    $query3="select * from driver where car_type_id='$car_type_id'  and online_offline = 1 and driver_admin_status=1 and busy=0 and login_logout=1";
    $result3 = $db->query($query3);
    $ex_rows=$result3->num_rows;

    if($ex_rows==0)
    {
        $image ="https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|".$pickup_lat.",".$pickup_long."&markers=color:red|label:D|".$drop_lat.",".$drop_long."&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4";
        $last_time_stamp = date("h:i:s A");
        $query1="INSERT INTO no_driver_ride_table(user_id,coupon_code, pickup_lat, pickup_long,pickup_location,drop_lat,drop_long,drop_location,
	ride_date,ride_time,ride_type,ride_status,ride_image,car_type_id,payment_option_id,card_id,last_time_stamp) 
	VALUES ('$user_id','$coupon_code','$pickup_lat','$pickup_long','$pickup_location','$drop_lat','$drop_long','$drop_location',
	'$date1','$time','1','1','$image','$car_type_id','$payment_option_id','$card_id','$last_time_stamp')";
	$db->query($query1);
        $re = array('result'=> 0,'msg'=> "No Driver Online",);
    }
    else {
        $query = "select * from admin_panel_settings WHERE admin_panel_setting_id=1";
        $result = $db->query($query);
        $admin_settings = $result->row;
        $admin_panel_firebase_id = $admin_settings['admin_panel_firebase_id'];
        $admin_panel_rideus_request = $admin_settings['admin_panel_rideus_request'];
        $admin_panel_request = $admin_settings['admin_panel_request'];
        $list3=$result3->rows;
        $c = array();
        foreach($list3 as $login3)
        {
            $driver_lat = $login3['current_lat'];
            $driver_long =$login3['current_long'];

            $theta = $pickup_long - $driver_long;
            $dist = sin(deg2rad($pickup_lat)) * sin(deg2rad($driver_lat)) +  cos(deg2rad($pickup_lat)) * cos(deg2rad($driver_lat)) * cos(deg2rad($theta));
            $dist = acos($dist);
            $dist = rad2deg($dist);
            $miles = $dist * 60 * 1.1515;
            $km=$miles* 1.609344;
            if($km <= $admin_panel_rideus_request)
            {
                $c[] = array("driver_id"	=> $login3['driver_id'],"distance" => $km,);
            }
        }

        if(!empty($c)){
            $image ="https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|".$pickup_lat.",".$pickup_long."&markers=color:red|label:D|".$drop_lat.",".$drop_long."&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4";
            $date = date("Y-m-d");
            $last_time_stamp = date("h:i:s A");
            $query1="INSERT INTO ride_table (user_id,coupon_code, pickup_lat, pickup_long,pickup_location,drop_lat,drop_long,drop_location,
	ride_date,ride_time,ride_type,ride_status,ride_image,car_type_id,payment_option_id,card_id,last_time_stamp,date,pem_file) 
	VALUES ('$user_id','$coupon_code','$pickup_lat','$pickup_long','$pickup_location','$drop_lat','$drop_long','$drop_location',
	'$date1','$time','1','1','$image','$car_type_id','$payment_option_id','$card_id','$last_time_stamp','$date','$pem_file')";

            $db->query($query1);
            $last_id = $db->getLastId();

            $query3="select * from ride_table where ride_id='$last_id'";
            $result3 = $db->query($query3);
            $list=$result3->row;
            $ride_status =$list['ride_status'];
            $ride_id = (string)$last_id;
            $nodes = array();
            if ($admin_panel_request == 1)
            {
                foreach($c as $driver){
                    $driver_id = $driver['driver_id'];
                    $nodes[$driver_id] = array('ride_id'=>$ride_id,'ride_status'=>"1");
                    $query3="select * from driver_ride_allocated where driver_id='$driver_id'";
                    $result3 = $db->query($query3);
                    $driver_allocated = $result3->row;
                    if (empty($driver_allocated))
                    {
                        $query5="INSERT INTO driver_ride_allocated (driver_id,ride_id,ride_mode) VALUES ('$driver_id','$last_id','1')";
                        $db->query($query5);
                    }else{
                        $query5="UPDATE driver_ride_allocated SET ride_id='$last_id' WHERE driver_id='$driver_id'" ;
                        $db->query($query5);
                    }
                    $query5="INSERT INTO ride_allocated (allocated_ride_id, allocated_driver_id,allocated_date) VALUES ('$last_id','$driver_id','$date')";
                    $db->query($query5);
                }
                new_ride($admin_panel_firebase_id,$nodes);
                foreach ($c as $driver){
                    $driver_id = $driver['driver_id'];
                    $query4="select * from driver where driver_id='$driver_id'";
                    $result4 = $db->query($query4);
                    $list4=$result4->row;
                    $device_id=$list4['device_id'];
                    $language="select * from messages where language_id=1 and message_id=32";
                    $lang_result = $db->query($language);
                    $lang_list=$lang_result->row;
                    $message=$lang_list['message_name'];
                    $ride_id= (String) $last_id;
                    $ride_status= (String) $ride_status;
                    if($device_id!="")
                    {
                        if($list4['flag'] == 1)
                        {
                            IphonePushNotificationDriver($device_id, $message,$ride_id,$ride_status,$pem_file);
                        }
                        else
                        {
                            AndroidPushNotificationDriver($device_id, $message,$ride_id,$ride_status);
                        }
                    }
                }
            }else{
                foreach ($c as $value)
                {
                    $distance[] = $value['distance'];
                }
                array_multisort($distance,SORT_ASC,$c);
                $driver_id = $c[0]['driver_id'];
                $nodes[$driver_id] = array('ride_id'=>$ride_id,'ride_status'=>"1");
                $query3="select * from driver_ride_allocated where driver_id='$driver_id'";
                $result3 = $db->query($query3);
                $driver_allocated = $result3->row;
                if (empty($driver_allocated))
                {
                    $query5="INSERT INTO driver_ride_allocated (driver_id,ride_id,ride_mode) VALUES ('$driver_id','$last_id','1')";
                    $db->query($query5);
                }else{
                    $query5="UPDATE driver_ride_allocated SET ride_id='$last_id' WHERE driver_id='$driver_id'" ;
                    $db->query($query5);
                }
                $query5="INSERT INTO ride_allocated (allocated_ride_id, allocated_driver_id,allocated_date) VALUES ('$last_id','$driver_id','$date')";
                $db->query($query5);
                new_ride($admin_panel_firebase_id,$nodes);
                $query4="select * from driver where driver_id='$driver_id'";
                $result4 = $db->query($query4);
                $list4=$result4->row;
                $device_id=$list4['device_id'];
                $language="select * from messages where language_id=1 and message_id=32";
                $lang_result = $db->query($language);
                $lang_list=$lang_result->row;
                $message=$lang_list['message_name'];
                $ride_id= (String) $last_id;
                $ride_status= (String) $ride_status;
                if($device_id!="")
                {
                    if($list4['flag'] == 1)
                    {
                        IphonePushNotificationDriver($device_id, $message,$ride_id,$ride_status,$pem_file);
                    }
                    else
                    {
                        AndroidPushNotificationDriver($device_id, $message,$ride_id,$ride_status);
                    }
                }
            }

            $query5="INSERT INTO table_user_rides(booking_id,ride_mode,user_id) VALUES ('$last_id','1','$user_id')";
            $db->query($query5);

            $query3="select * from ride_table where ride_id='$last_id'";
            $result3 = $db->query($query3);
            $list=$result3->row;
            $re = array('result'=> 1,'msg'=> "".$km,'details'=> $list);

        }else{
            $image ="https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|".$pickup_lat.",".$pickup_long."&markers=color:red|label:D|".$drop_lat.",".$drop_long."&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4";
            $last_time_stamp = date("h:i:s A");
            $query1="INSERT INTO no_driver_ride_table (user_id,coupon_code, pickup_lat, pickup_long,pickup_location,drop_lat,drop_long,drop_location,
	ride_date,ride_time,ride_type,ride_status,ride_image,car_type_id,payment_option_id,card_id,last_time_stamp) 
	VALUES ('$user_id','$coupon_code','$pickup_lat','$pickup_long','$pickup_location','$drop_lat','$drop_long','$drop_location',
	'$date','$time','1','1','$image','$car_type_id','$payment_option_id','$card_id','$last_time_stamp')";
            $db->query($query1);
            $re = array('result'=> 0,'msg'=> "No Nearest Driver",);
        }

    }

}
else
{
    $re = array('result'=> 0,'msg'=>"Required Field Missing");
}
$log  = "car by city api -: ".date("F j, Y, g:i a").PHP_EOL.
        "Response: ".json_encode($re).PHP_EOL.
        "drop_lat: ".$drop_lat.PHP_EOL.
        "drop_long: ".$drop_long.PHP_EOL.
        "drop_location: ".$drop_location.PHP_EOL.
        "car_type_id:".$car_type_id.PHP_EOL.
        "user_id:".$user_id.PHP_EOL.
        "coupon_code :". $coupon_code.PHP_EOL.
        "pickup_lat : ".$pickup_lat.PHP_EOL.
        "pickup_long : ". $pickup_long.PHP_EOL.
        "pickup_location :".$pickup_location.PHP_EOL.
        "payment_option_id :".$payment_option_id.PHP_EOL.
        "card_id :".$card_id.PHP_EOL.
        "pem_file :".$pem_file.PHP_EOL.
        "-------------------------".PHP_EOL;

file_put_contents('../logfile/log_'.date("j.n.Y").'.txt', $log, FILE_APPEND);
echo json_encode($re, JSON_PRETTY_PRINT);
?>